"use strict";
const database = require('../database');
module.exports = class UserFactory {

    static async  getProducts(id){
        const query = `SELECT p.name from transactions t INNER JOIN products p ON t.product_id = p.id where user_id = ? `;
        const values = [id];
        const productsBot = await database.selectAll({query, values});
        return productsBot;
	}
    static async  getSalaries(id){
        const query = `SELECT * from salaries where user_id = ?`;
        const values = [id];
        const salaries = await database.selectAll({query, values});
        return salaries;
    }
    static async getUserBalance(id){
        const salaryQuery = "SELECT SUM(amount) from salaries";
        const values = [id];
        const salaryTotal =  await database.select({salaryQuery, values});
        const transactionsSumWithDiscountQuery = "SELECT SUM(p.price - (p.price*(d.discount/100)))\n" +
            "from transactions t\n" +
            " INNER JOIN products p \n" +
            " ON t.product_id = p.id \n" +
            " LEFT JOIN discounts d \n" +
            " ON t.product_id = d.product\n" +
            " \n" +
            " where t.user_id = 1\n" +
            "\n";
        const transactionsSumWithDiscountQuery =  await database.select({transactionsSumWithDiscount, values});
        return salaryQuery- transactionsSumWithDiscountQuery > 0 ? salaryQuery- transactionsSumWithDiscountQuery : 0;
    }

};
