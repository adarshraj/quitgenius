"use strict";
const userFactory = require('./UserFactory');
const database = require('../database').connect();

module.exports = class User {

    static async getProducts(userId){
        try {
            const productsBot = await userFactory.getProducts(userId);
            return productsBot.length > 0 ? [null, productsBot] : [null, []]
        }catch(e){
            return [new Error(e.message), []];
        }
    }
    static async salaries(userId){
        try {
            const salaries = await userFactory.getSalaries(userId);
            return salaries.length > 0 ? [null, salaries] : [null, []]
        }catch(e){
            return [new Error(e.message), []];
        }
    }
    static async balance(userId){
        try {
            const balance = await userFactory.getUserBalance(userId);
            return balance !=null  ? [null, balance] : [null, 0]
        }catch(e){
            return [new Error(e.message), null];
        }
    }
};
