const mysql = require('promise-mysql');
const configs = require('./config').configs;
let connection;

const dataBase = {
    connect: async () => {
        connection = await mysql.createConnection({
            host: configs.MYSQL_HOST,
            user: configs.MYSQL_USER,
            password: configs.MYSQL_PASSWORD,
            database: configs.MYSQL_DATABASE,
        });
        return connection;
    },
    select: async ({ query, values }) => {
        const [rows] = await connection.query(query, values);
        return rows;
    },
    selectAll: async ({ query, values }) => await connection.query(query, values),
};
module.exports = dataBase;

