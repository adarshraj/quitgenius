require('dotenv').config();
const Joi = require('joi');

const configSchema = Joi.object({
    MYSQL_HOST: Joi.string().required(),
    MYSQL_DATABASE: Joi.string().required(),
    MYSQL_USER: Joi.string(),
    MYSQL_PASSWORD: Joi.string()
}).unknown().required();

const { error, value: configs } = Joi.validate(process.env, configSchema);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}
exports.configs = configs;
